package ru.t1.chubarov.tm.command.system;

public final class ApplicationExitCommand  extends AbstractSystemCommand{

    @Override
    public void execute() { System.exit(0);  }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Close application";
    }

}
