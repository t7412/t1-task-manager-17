package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectid = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectid);
        renderTask(tasks);
    }

    @Override
    public String getName() { return "task-show-by-index"; }

    @Override
    public String getDescription() { return "Display task by index."; }

}
