package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY INDEX");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeByIndex(index);
    }

    @Override
    public String getName() { return "task-remove-by-index"; }

    @Override
    public String getDescription() { return "Remove task by index."; }

}
