package ru.t1.chubarov.tm.component;

import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.command.project.*;
import ru.t1.chubarov.tm.command.system.*;
import ru.t1.chubarov.tm.command.task.*;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.chubarov.tm.exception.system.CommandNotSupportedException;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.repository.CommandRepository;
import ru.t1.chubarov.tm.repository.ProjectRepository;
import ru.t1.chubarov.tm.repository.TaskRepository;
import ru.t1.chubarov.tm.service.*;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    @Override
    public IProjectService getProjectService() { return projectService; }
    @Override
    public ITaskService getTaskService() { return taskService; }
    @Override
    public IProjectTaskService getProjectTaskService() { return projectTaskService; }
    @Override
    public ICommandService getCommandService() { return commandService; }
    @Override
    public ILoggerService getLoggerService() { return loggerService; }

    {
        regestry(new ApplicationAboutCommand());
        regestry(new ApplicationExitCommand());
        regestry(new ApplicationHelpCommand());
        regestry(new ApplicationVersionCommand());
        regestry(new ArgumentListCommand());
        regestry(new CommandListCommand());
        regestry(new SystemInfoCommand());

        regestry(new ProjectShowCommand());
        regestry(new ProjectChangeStatusByIdCommand());
        regestry(new ProjectChangeStatusByIndexCommand());
        regestry(new ProjectCompleteByIdCommand());
        regestry(new ProjectCompleteByIndexCommand());
        regestry(new ProjectCreateCommand());
        regestry(new ProjectRemoveByIdCommand());
        regestry(new ProjectRemoveByIndexCommand());
        regestry(new ProjectsClearCommand());
        regestry(new ProjectShowByIdCommand());
        regestry(new ProjectShowByIndexCommand());
        regestry(new ProjectStartByIdCommand());
        regestry(new ProjectStartByIndexCommand());
        regestry(new ProjectUpdateByIdCommand());
        regestry(new ProjectUpdateByIndexCommand());
        regestry(new ProjectTaskBindToProjectCommand());
        regestry(new ProjectTaskUnbindToProjectCommand());

        regestry(new TaskShowCommand());
        regestry(new TaskChangeStatusByIdCommand());
        regestry(new TaskChangeStatusByIndexCommand());
        regestry(new TaskClearCommand());
        regestry(new TaskCompleteByIdCommand());
        regestry(new TaskCompleteByIndexCommand());
        regestry(new TaskCreateCommand());
        regestry(new TaskRemoveByIdCommand());
        regestry(new TaskRemoveByIndexCommand());
        regestry(new TaskShowByIdCommand());
        regestry(new TaskShowByIndexCommand());
        regestry(new TaskShowByProjectIdCommand());
        regestry(new TaskStartByIdCommand());
        regestry(new TaskStartByIndexCommand());
        regestry(new TaskUpdateByIdCommand());
        regestry(new TaskUpdateByIndexCommand());
    }

    private void initDemoData() {
        projectService.add(new Project( "TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project( "ALFA PROJECT", Status.NOT_STARTED));
        projectService.add(new Project( "BETA PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project( "DELTA PROJECT", Status.COMPLETED));
        taskService.create("FIRST TASK", "FIRST DESCRIPTION");
        taskService.create("SECOND TASK", "SECOND DESCRIPTION");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    private void regestry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(final String[] args) throws AbstractException {
        if (processArguments(args)) System.exit(0);
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()){
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            }
            catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private boolean processArguments(final String[] args) throws AbstractException {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    private void processArgument(final String arg) throws AbstractException {
        final AbstractCommand abstractCommand = commandService.getCommandByName(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(final String command) throws AbstractException {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

}
