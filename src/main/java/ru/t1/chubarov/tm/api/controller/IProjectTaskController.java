package ru.t1.chubarov.tm.api.controller;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.entity.TaskNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.chubarov.tm.exception.field.TaskIdEmptyException;

public interface IProjectTaskController {

    void bindTaskToProject() throws AbstractException;

    void unbindTaskFromProject() throws AbstractException;
}
