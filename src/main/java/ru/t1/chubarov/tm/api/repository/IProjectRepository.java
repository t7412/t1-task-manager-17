package ru.t1.chubarov.tm.api.repository;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    void clear();

    Project findOneById(String id) throws AbstractException;

    Project findOneByIndex(Integer index) throws AbstractException;

    void remove(Project project);

    Project removeById(String id) throws AbstractException;

    Project removeByIndex(Integer index) throws IndexIncorrectException;

    Integer getSize();

    boolean existsById(String id);

}
